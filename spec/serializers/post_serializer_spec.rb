require 'rails_helper'

RSpec.describe PostSerializer do
  let(:user)          { FactoryBot.create(:user) }
  let(:post)          { FactoryBot.create(:post, user_id: user.id) }
  let(:serializer)    { described_class.new(post) }
  let(:serialization) { ActiveModelSerializers::Adapter.create(serializer).as_json }

  it 'includes the expected attributes' do
    expect(serialization.keys).to contain_exactly(:id, :title, :body, :created_at, :updated_at, :user, :user_id)
  end
end
