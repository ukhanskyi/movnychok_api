require 'rails_helper'

USERS_TAG = 'Users'.freeze

RSpec.describe 'Api::V1::Users::RegistrationsController', type: :request do # rubocop:disable Metrics/BlockLength
  let(:user) { FactoryBot.create(:user) }

  path '/signup' do # rubocop:disable Metrics/BlockLength
    post 'Registration new User' do # rubocop:disable Metrics/BlockLength
      tags USERS_TAG

      consumes 'application/json'
      produces 'application/json'

      parameter name: :user, in: :body, schema: {
        type: :object,
        properties: {
          user: {
            type: :object,
            properties: {
              email: { type: :string },
              password: { type: :string },
              name: { type: :string }
            },
            required: %w[email password name]
          }
        },
        required: [:user]
      }

      response(200, 'user created successfully') do
        let(:user) { FactoryBot.attributes_for(:user) }

        it 'creates new user' do
          post '/signup', params: { user: }

          data = response.parsed_body

          expect(data['status']['code']).to eq(200)
          expect(data['status']['message']).to eq('Signed up sucessfully.')
          expect(data['data']['email']).to eq(user[:email])
        end
      end

      response(422, 'unprocessable entity') do
        let(:user) { FactoryBot.attributes_for(:user, name: '') }

        it 'returns error' do
          post '/signup', params: { user: }

          data = response.parsed_body

          expect(data['status']['code']).to eq(422)
          expect(data['status']['message']).to eq("User couldn't be created successfully. Name can't be blank")
        end
      end
    end

    delete 'Delete registered user' do
      tags USERS_TAG

      consumes 'application/json'
      produces 'application/json'

      response(200, 'Account deleted successfully') do
        let(:Authorization) { auth_token(user) }

        before { delete '/signup', headers: { Authorization: auth_token(user) } }

        it 'deletes the user' do
          expect(response).to have_http_status(:ok)
          expect(response.body).to include('Account deleted successfully.')
        end
      end
    end
  end
end
