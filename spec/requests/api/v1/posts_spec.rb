require 'swagger_helper'

POSTS_TAG = 'Posts'.freeze

RSpec.describe 'Api::V1::PostsController', type: :request do # rubocop:disable Metrics/BlockLength
  let(:user)          { FactoryBot.create(:user) }
  let(:Authorization) { auth_token(user) }
  let(:my_post)       { FactoryBot.create(:post, user_id: user.id) }

  path '/api/v1/posts' do # rubocop:disable Metrics/BlockLength
    get 'Return posts sorted by DESC (Index page)' do
      tags POSTS_TAG
      security [jwt: []]

      consumes 'application/json'
      produces 'application/json'

      response(200, 'successful') do
        let!(:posts) { FactoryBot.create_list(:post, 3) }

        it 'returns 3 posts' do
          get api_v1_posts_url, headers: { Authorization: auth_token(user) }

          data = response.parsed_body

          expect(data.count).to eq(3)
          expect(response).to have_http_status(:success)
        end
      end
    end

    post 'Creates a new post' do # rubocop:disable Metrics/BlockLength
      tags POSTS_TAG
      security [jwt: []]

      consumes 'application/json'
      produces 'application/json'

      parameter name: :new_post, in: :body, schema: {
        type: :object,
        properties: {
          post: {
            type: :object,
            properties: {
              title: { type: :string },
              body: { type: :string }
            },
            required: %w[title body]
          }
        },
        required: [:post]
      }

      response(201, 'post created successfully') do
        let!(:new_post) { FactoryBot.attributes_for(:post, title: 'New Post Title') }

        it 'creates new post' do
          post api_v1_posts_url, params: { post: new_post }, headers: { Authorization: auth_token(user) }

          data = response.parsed_body

          expect(data['title']).to eq('New Post Title')
          expect(response).to have_http_status(:created)
        end
      end

      response(422, 'unprocessable entity') do
        let(:new_post) { FactoryBot.attributes_for(:post, title: nil) }

        it 'doesn`t create a new post with invalid attributes' do
          post api_v1_posts_url, params: { post: new_post }, headers: { Authorization: auth_token(user) }

          expect(response).to have_http_status(:unprocessable_entity)
        end
      end

      response(401, 'unauthorized') do
        let(:new_post) { FactoryBot.attributes_for(:post) }

        it 'doesn`t create a new post because you are unauthorized' do
          post api_v1_posts_url, params: { post: new_post }

          expect(response).to have_http_status(:unauthorized)
          expect(response.body).to include('You need to sign in or sign up before continuing.')
        end
      end
    end
  end

  path '/api/v1/posts/{id}' do # rubocop:disable Metrics/BlockLength
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get 'Find post by ID' do # rubocop:disable Metrics/BlockLength
      tags POSTS_TAG
      security [jwt: []]

      consumes 'application/json'
      produces 'application/json'

      response(200, 'successful') do
        it 'renders a successful show page response' do
          get api_v1_post_url(my_post.id), headers: { Authorization: auth_token(user) }

          data = response.parsed_body

          expect(data['id']).to eq(my_post.id)
          expect(response).to have_http_status(:success)
        end
      end

      response(404, 'not found') do
        let(:id) { '000' }

        it 'doesn`t render show page response with invalid id' do
          get api_v1_post_url(id), headers: { Authorization: auth_token(user) }

          expect(response).to have_http_status(:not_found)
        end
      end

      response(401, 'unauthorized') do
        it 'doesn`t create a new post because you are unauthorized' do
          get api_v1_post_url(my_post.id)

          expect(response).to have_http_status(:unauthorized)
          expect(response.body).to include('You need to sign in or sign up before continuing.')
        end
      end
    end

    patch 'Update existing post partially' do # rubocop:disable Metrics/BlockLength
      tags POSTS_TAG
      security [jwt: []]

      consumes 'application/json'
      produces 'application/json'

      parameter name: :post_data, in: :body, schema: {
        type: :object,
        properties: {
          post: {
            type: :object,
            properties: {
              title: { type: :string },
              body: { type: :string },
              user_id: { type: :integer }
            },
            required: %w[title body user_id]
          }
        },
        required: [:post]
      }

      response(200, 'successful') do
        let(:post_data) { FactoryBot.attributes_for(:post, title: 'Updated post title with PATCH', user_id: user.id) }

        it 'updates post with PATCH and valid attributes' do
          patch api_v1_post_url(my_post.id),
                params: { post: post_data },
                headers: { Authorization: auth_token(user) }

          data = response.parsed_body

          expect(data['title']).to eq('Updated post title with PATCH')
          expect(response).to have_http_status(:success)
        end
      end

      response(401, 'unauthorized') do
        let(:post_data) { FactoryBot.attributes_for(:post, title: 'Updated post title with PATCH', user_id: user.id) }

        it 'doesn`t update a new post because you are unauthorized' do
          patch api_v1_post_url(my_post.id), params: { post: post_data }

          expect(response).to have_http_status(:unauthorized)
          expect(response.body).to include('You need to sign in or sign up before continuing.')
        end
      end

      response(404, 'not found') do
        let(:post_data) { FactoryBot.attributes_for(:post, title: 'Updated post title with PATCH', user_id: user.id) }

        it 'doesn`t update post because post does not exist' do
          patch api_v1_post_url('000'),
                params: { post: post_data },
                headers: { Authorization: auth_token(user) }

          expect(response).to have_http_status(:not_found)
        end
      end

      response(422, 'unprocessable entity') do
        let(:post_data) { FactoryBot.attributes_for(:post, title: nil, user_id: user.id) }

        it 'doesn`t update post with PATCH method and invalid attributes' do
          patch api_v1_post_url(my_post.id),
                params: { post: post_data },
                headers: { Authorization: auth_token(user) }

          expect(response).to have_http_status(:unprocessable_entity)
        end
      end
    end

    put 'Update an existing post or create new if post doesn`t exist' do # rubocop:disable Metrics/BlockLength
      tags POSTS_TAG
      security [jwt: []]

      consumes 'application/json'
      produces 'application/json'

      parameter name: :post_data, in: :body, schema: {
        type: :object,
        properties: {
          post: {
            type: :object,
            properties: {
              title: { type: :string },
              body: { type: :string },
              user_id: { type: :integer }
            },
            required: %w[title body user_id]
          }
        },
        required: [:post]
      }

      response(200, 'successful') do
        let(:post_data) { FactoryBot.attributes_for(:post, title: 'Updated post title with PUT', user_id: user.id) }

        it 'updates post with PATCH and valid attributes' do
          put api_v1_post_url(my_post.id),
              params: { post: post_data },
              headers: { Authorization: auth_token(user) }

          data = response.parsed_body

          expect(data['title']).to eq('Updated post title with PUT')
          expect(response).to have_http_status(:success)
        end
      end

      response(401, 'unauthorized') do
        let(:post_data) { FactoryBot.attributes_for(:post, title: 'Updated post title with PUT', user_id: user.id) }

        it 'doesn`t update post because you are unauthorized' do
          put api_v1_post_url(my_post.id), params: { post: post_data }

          expect(response).to have_http_status(:unauthorized)
          expect(response.body).to include('You need to sign in or sign up before continuing.')
        end
      end

      response(404, 'not found') do
        let(:post_data) { FactoryBot.attributes_for(:post, title: 'Updated post title with PUT', user_id: user.id) }

        it 'doesn`t update post because post does not exist' do
          put api_v1_post_url('000'),
              params: { post: post_data },
              headers: { Authorization: auth_token(user) }

          expect(response).to have_http_status(:not_found)
        end
      end

      response(422, 'unprocessable entity') do
        let(:post_data) { FactoryBot.attributes_for(:post, title: nil, user_id: user.id) }

        it 'doesn`t update post with PATCH method and invalid attributes' do
          put api_v1_post_url(my_post.id),
              params: { post: post_data },
              headers: { Authorization: auth_token(user) }

          expect(response).to have_http_status(:unprocessable_entity)
        end
      end
    end

    delete 'Delete a post' do
      tags POSTS_TAG
      security [jwt: []]

      consumes 'application/json'
      produces 'application/json'

      response(204, 'successful') do
        it 'deletes a post successfuly' do
          delete api_v1_post_url(my_post.id),
                 headers: { Authorization: auth_token(user) }

          expect(response.body).to be_empty
        end
      end

      response(401, 'unauthorized') do
        it 'doesn`t delete post because you are unauthorized' do
          delete api_v1_post_url(my_post.id)

          expect(response).to have_http_status(:unauthorized)
          expect(response.body).to include('You need to sign in or sign up before continuing.')
        end
      end

      response(404, 'not found') do
        it 'doesn`t delete post because post does not exist' do
          delete api_v1_post_url('000'),
                 headers: { Authorization: auth_token(user) }

          expect(response).to have_http_status(:not_found)
        end
      end
    end
  end
end
