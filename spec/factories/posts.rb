# Users factory
FactoryBot.define do
  factory :post do
    title { Faker::Lorem.sentence(word_count: rand(5..15)) }
    body  { Faker::Lorem.paragraphs(number: rand(3..5)).join("\n\n") }

    association :user, factory: :user
  end
end
