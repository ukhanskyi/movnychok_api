require 'devise/jwt/test_helpers'

# Helper for specs to test functional with authorized user
module AuthHelper
  def auth_token(user)
    auth_headers = Devise::JWT::TestHelpers.auth_headers({}, user)
    auth_headers['Authorization']
  end
end
