# Helper to work with API(parse json and auth)
module ApiHelpers
  def json
    JSON.parse(response.body)
  end

  def login_with_api(user)
    post '/login', params: {
      user: {
        email: user.email,
        password: user.password
      }
    }
  end
end
