# frozen_string_literal: true

# API module
module Api
  # V1 Module
  module V1
    # Users Module
    module Users
      # Registrations
      class RegistrationsController < Devise::RegistrationsController
        include RackSessionFix

        respond_to :json

        before_action :configure_permitted_parameters

        protected

        def configure_permitted_parameters
          added_attrs = [:name]
          devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
          devise_parameter_sanitizer.permit :account_update, keys: added_attrs
        end

        private

        def respond_with(resource, _opts = {})
          if request.method == 'POST' && resource.persisted?
            render_created_response(resource)
          elsif request.method == 'DELETE'
            render_deleted_response
          else
            render_error_response(resource)
          end
        end

        def render_created_response(resource)
          render json: {
            status: { code: 200, message: 'Signed up sucessfully.' },
            data: resource
          }, status: :ok
        end

        def render_deleted_response
          render json: { status: { code: 200, message: 'Account deleted successfully.' } }, status: :ok
        end

        def render_error_response(resource)
          render json: {
            status: {
              code: 422,
              message: "User couldn't be created successfully. #{resource.errors.full_messages.to_sentence}"
            }
          }, status: :unprocessable_entity
        end
      end
    end
  end
end
