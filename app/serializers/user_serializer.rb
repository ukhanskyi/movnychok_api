# User data that could be sent
class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :name, :created_at, :updated_at
  has_many :posts
end
