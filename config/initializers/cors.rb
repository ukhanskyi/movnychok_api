# frozen_string_literal: true

# Be sure to restart your server when you modify this file.

# Avoid CORS issues when API is called from the frontend app.
# Handle Cross-Origin Resource Sharing (CORS) in order to accept cross-origin Ajax requests.

# Read more: https://github.com/cyu/rack-cors

Rails.application.config.middleware.insert_before 0, Rack::Cors do
  allow do
    origins 'http://127.0.0.1:3001', 'http://localhost:3001', 'http://0.0.0.0:3001', # For Local
            'http://127.0.0.1:3002', 'http://localhost:3002', 'http://0.0.0.0:3002', # For Docker
            'http://127.0.0.1:5173', 'http://localhost:5173', 'http://0.0.0.0:5173'  # For React.js client

    resource '*',
             headers: :any,
             expose: %w[Authorization],
             methods: %i[get post put patch delete options head]
  end
end
